package ru.kpfu.mvgavrilov;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Input n = ");
        n = in.nextInt();
        int[][] m = new int[n][n];
        for (int i = 0;i<n;i++){
            for (int j = 0;j<n;j++){
                System.out.print("m["+i+"]"+"["+j+"] = ");
                m[i][j] = in.nextInt();
            }
        }
        for (int i = 0;i<n;i++){
            for (int j = 0;j<n;j++){
                if(i>j) {m[i][j] = 0;}System.out.print(m[i][j]+" ");
            }
            System.out.println();
        }
    }
}
