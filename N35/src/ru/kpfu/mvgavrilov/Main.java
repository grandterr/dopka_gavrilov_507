package ru.kpfu.mvgavrilov;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){

        // 34 и 35 выполняются

        Scanner in = new Scanner(System.in);
        int n,max;
        max = -9999999;
        System.out.print("Число элементов массива = ");
        n = in.nextInt();
        int[] a = new int[n];
        for(int i = 0;i<n;i++){
            System.out.print("a["+i+"] = ");
            a[i] = in.nextInt();
        }
        for(int i = 0;i<n-2;i++){
            if (a[i]+a[i+1]+a[i+2]>max) {max = a[i]+a[i+1]+a[i+2];}
        }
        System.out.println("Max 3p = "+max);
        System.out.println("========SORTED MASSIVE========="); //сортировка массива в задаче Т035
        for (int i = 0; i<a.length;i++){
            int min = a[i];
            int imin = i;
            for(int j = i+1;j<a.length;j++) {
                if (a[j]< min) {
                    min = a[j];
                    imin = j;
                }
            }
            if (i != imin) {
                int temp = a[i];
                a[i] = a[imin];
                a[imin] = temp;
            }
        }
        for (int i = 0;i<a.length;i++){
            System.out.println("a["+i+"] = "+a[i]);
        }
    }
}
