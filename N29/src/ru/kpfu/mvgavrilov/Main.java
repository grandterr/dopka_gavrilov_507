package ru.kpfu.mvgavrilov;

import java.util.Scanner;
public class Main{
    static String K9(int k,int n){
        if (n==0) return "";
        String c;
        c=(n%k)+"";
        return K9(k,n/k)+c;
    }
    static int REK9(int k,int n){
        if (n==0) return 0;
        int c;
        int r;
        r = 0;
        c=n%k;
        r=c*10;
        return REK9(k,n/10)+r;
    }
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int k,n;
        System.out.print("Input n = ");
        n = in.nextInt();
        System.out.print("Input k = ");
        k = in.nextInt();
        if ((2<=k) && (k<=9)) {System.out.println("Number in "+k+"system = "+K9(k,n));
            System.out.println("Number in "+10+"system = "+REK9(10,n));
        }
    }
}