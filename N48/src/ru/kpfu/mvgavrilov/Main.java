package ru.kpfu.mvgavrilov;


public class Main {
    public static void main(String[] args) {
        Student s = new Student("Гаврилов МВ", 1995);
        Teacher v = new Teacher("Абрамский ММ", "Программирование");
        System.out.println(v.getFio());
        v.setFio("");
        System.out.println(v.getSubject());
        v.setSubject("");
        v.evaluateMark(s);
    }
}