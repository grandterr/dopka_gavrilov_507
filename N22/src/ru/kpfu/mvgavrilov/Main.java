package ru.kpfu.mvgavrilov;

import java.util.Scanner;
public class Main{
    public static void main(String[] args){
        int n;
        int c;
        Scanner in = new Scanner(System.in);
        System.out.print("Input n = ");
        n = in.nextInt();
        char[][] m = new char[n*2+1][n*2+1];
        for(int y =0; y<n*2+1;y++){
            for(int x =0; x<n*2+1;x++){
                if((y+1<=x)&&(x<=(n*2+1)-y-2)&&(x<=(n*2+1)/2)) {m[y][x] = '0';}
                else { if(((n*2+1)-y<=x)&&(x<=y-1)&&(y>=(n*2+1)/2)) {m[y][x] = '0';}

                else {m[y][x] = '*';}
                }
                System.out.print(m[y][x]);
            }
            System.out.println();
        }
    }
}
