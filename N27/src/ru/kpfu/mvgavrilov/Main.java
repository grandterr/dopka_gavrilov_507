package ru.kpfu.mvgavrilov;

import java.util.Scanner;
public class Main{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n;
        int x;
        int d;
        System.out.print("Input n = ");
        n = in.nextInt();
        for (int i = 1; i<=n;i++){
            System.out.print("Input x = ");
            x = in.nextInt();
            if ((x%2 ==0 && x%3 == 0) || (x%5==0 && x%6==0)) { d = x; System.out.println("Number = "+d); }
        }
    }
}