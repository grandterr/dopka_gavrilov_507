package ru.kpfu.mvgavrilov;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Input n = ");
        n = in.nextInt();
        int[][] m = new int[n][n];
        for (int i = 0;i<n;i++){
            for (int j = 0;j<n;j++){
                m[i][j] = i+1;
                System.out.print(m[i][j]);
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        for (int i = 0;i<n;i++){
            for (int j = 0;j<n;j++){
                if((i+1<=j)&&(j<=n-i-2)&&(i<=n/2)) {m[i][j] = 0;}
                else { if((n-i<=j)&&(j<=i-1)&&(i>=n/2)) {m[i][j] = 0;}
                }
                System.out.print(m[i][j]);
            }
            System.out.print("\n");
        }
    }
}
