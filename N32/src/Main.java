
import java.util.Scanner;


public class Main {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n;
        System.out.print("число элементов массива = ");
        n = in.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        int[] c = new int[n];
        for(int i = 0;i<n;i++){
            System.out.print("a["+i+"] = ");
            a[i] = in.nextInt();
            System.out.print("b["+i+"] = ");
            b[i] = in.nextInt();
            c[i] = a[i]+b[i];
        }
        System.out.println("-------------------");
        for(int i = 0;i<n;i++){
            System.out.println("c["+i+"] = "+c[i]);
        }
    }
}
