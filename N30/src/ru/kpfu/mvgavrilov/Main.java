package ru.kpfu.mvgavrilov;

import java.util.Scanner;
public class Main{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n,x,k,d,g;
        d = 0;
        g = 0;
        System.out.print("Input n = ");
        n = in.nextInt();
        for (int i = 1;i<=n;i++){
            System.out.print("Input x = ");
            x = in.nextInt();
            for(int j = -1; j<=x;j++){
                k = x%10;
                if (k%2==0) {d+=1;}
                x= x/10;
            }
            if (d==3 || d==5 || d==0) {g+=1;}
            d = 0;
        }
        if (g==2) {System.out.println("YES");}
        else {System.out.println("NO");}
    }
}
