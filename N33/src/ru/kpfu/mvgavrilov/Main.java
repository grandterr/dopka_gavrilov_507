package ru.kpfu.mvgavrilov;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Input n = ");
        n = in.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        double[] sp = new double[n];
        for(int i = 0;i<n;i++){
            System.out.print("a["+i+"] = ");
            a[i] = in.nextInt();
            System.out.print("b["+i+"] = ");
            b[i] = in.nextInt();
            sp[i]= a[i]*b[i]*Math.cos(a[i]*b[i]);
        }
        System.out.print("\n");
        for(int i = 0;i<n;i++){
            System.out.println("sp["+i+"] = "+sp[i]);
        }
    }
}
