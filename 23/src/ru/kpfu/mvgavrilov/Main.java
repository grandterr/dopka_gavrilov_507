package ru.kpfu.mvgavrilov;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n;
        double EPS=1E-7;
        int s; s = 0;
        int st2=1,f=1,sn=0;
        int i =1;
        boolean stop=false;
        while((sn>EPS)||(i==1)){
            sn = ((i+1)*st2)/f;
            s+=sn;
            st2*=2;
            f*=2*i*(2*i+1);
            i++;
            System.out.println("S= "+sn);
        }
        System.out.println();
        System.out.print("RESULT: ");
        System.out.println("Summ = "+ s + "Step number = " + i);
    }
}