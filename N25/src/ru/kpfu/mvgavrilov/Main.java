package ru.kpfu.mvgavrilov;

import java.util.Scanner;
public class Main {
    static int T(int k){
        if (k%2==0){
            return 1;}
        else {return -1;}
    }
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n;
        double s;
        s = 0.000000;
        System.out.print("Input n = ");
        n = in.nextInt();
        for(int i = 1; i<=n;i++){
            s+=(double)T(n+1)/(n*n+3*n);
        }
        System.out.println("ANSWER = "+s);
    }
}